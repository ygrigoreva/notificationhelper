package com.ygrigoreva.notificationsample;

import android.app.Activity;
import android.os.Bundle;

import com.ygrigoreva.notificationhelper.NotificationHelper;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        NotificationHelper helper = NotificationHelper.init(getApplicationContext());

        helper.buildNotification(R.mipmap.ic_launcher, "Test title", "Test content");

    }
}
