package com.ygrigoreva.notificationhelper;

import android.app.Notification;
import android.app.NotificationManager;
import android.content.Context;
import android.support.v4.app.NotificationCompat;

/**
 * Created by Yulia Grigorieva (grigorieva@zingaya.com) on 24.01.2017.
 */

public class NotificationHelper {

    private static NotificationHelper instance = null;
    private static NotificationManager notificationManager;
    private static int notificationId = 0;
    private final Context appContext;

    private NotificationHelper(Context context) {
        notificationManager = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);
        appContext = context;
    }

    public static NotificationHelper init(Context context) {
        if (instance == null) {
            instance = new NotificationHelper(context);
        }
        return instance;
    }

    public static NotificationHelper get() {
        if (instance == null) {
            throw new IllegalStateException("ForegroundCheck is not initialized");
        }
        return instance;
    }

    public int buildNotification(int icon, String title, String content) {
        notificationId++;
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(appContext)
                .setSmallIcon(icon)
                .setContentTitle(title)
                .setContentText(content)
                .setPriority(Notification.PRIORITY_HIGH)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 })
                .setAutoCancel(true);

        NotificationManager mNotificationManager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);

        mNotificationManager.notify(notificationId, mBuilder.build());

        return notificationId;
    }

    public static void cancelNotification(int notificationId) {
        if (notificationId > -1) {
            notificationManager.cancel(notificationId);
        }
    }

}

